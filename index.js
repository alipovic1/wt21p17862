var express = require('express');
var app = express();
var path = require('path');
const bodyParser = require("body-parser");
const fs = require("fs");
const { parse } = require("path");
const { json } = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const db = require('./baza.js');
const Sequelize = require('sequelize');
module.exports = Sequelize;

app.use(express.static(path.join(__dirname, 'public/html')));
app.use(express.static(path.join(__dirname, 'public/css')));
app.use(express.static(path.join(__dirname, 'public/js')));
app.use(express.static(path.join(__dirname, 'public/images')));
app.use(express.static(path.join(__dirname, 'modeli')));
app.use(express.static("public"));
app.use(bodyParser.text());  

const Student = require('./modeli/student.js');
const Grupa = require('./modeli/grupa.js');
const Vjezba = require('./modeli/vjezba.js');
const Zadatak = require('./modeli/zadatak.js');


////////////////////////////////////////////// POST

app.post('/student', function(req,res) {

    const imee = req.body['ime'];
    const prezimee = req.body['prezime'];
    const indexx = req.body['index'];
    const grupaa = req.body['grupa'];


    db.Student.findOne({where: {index: indexx}}).then(function(student){
        if(!student){
            db.Student.create({ime: imee, prezime: prezimee, index: indexx}).then(function(){
                db.Grupa.findOne({where: {naziv: grupaa}}).then(function(grupica){
                 //   console.log("Grupa " + grupica);
                    if(!grupica) {
                        db.Grupa.create({naziv : grupaa}).then((g) => {
                            db.Student.update({ grupa: g.id },{ where: { index : indexx } }).then(()=> {
                                res.send({status: "Kreiran student!"});
                            })
                        })
                    } 
                        
                    })
                })
        }
        else{
            res.send({status: "Student sa indexom " + indexx + " već postoji!"});
        }
    }).catch(function(err){
        console.log(err)
    })
})
 
    
//////////////////////////////////////////////// PUT 
app.put('/student/:index', function(req, res){

    const indeksStudenta = req.params.index;
    const nazivGrupe = req.body.grupa;
    

    db.Student.findOne({ where : {index : indeksStudenta}}).then(function(student){
        if (!student) {
            res.send({status: "Student sa indexom " + indeksStudenta + " ne postoji!"});
        }
        else {
            db.Grupa.findOne({ where : {naziv : nazivGrupe}}).then(function(grupica){
                if (grupica) {
                    db.Student.update({grupa : grupica.id}, {where : {index : indeksStudenta}}).then(() => {
                        res.send({status: "Promijenjena grupa studentu " + indeksStudenta});
                    })
                }
                else {
                    db.Grupa.create({naziv : nazivGrupe}).then(() => {
                        db.Student.update({ grupa: g.id },{ where: { index : indeksStudenta }}).then(() => {
                            res.send({status : "Promijenjena grupa studentu " + indeksStudenta});
                        });
                    })
                }
            })
        }
    })
});

/////////////////////////////////////////////////////////////////// POST CSV        
app.post('/batch/student', function(req, res) {

        var studenti = req.body.toString('utf-8').split("\n");
    
        Promise.all(studenti.map(red => {
            var studentNiz = red.split(",");
            var studentBatch = {
                ime: studentNiz[0],
                prezime: studentNiz[1],
                index: studentNiz[2],
                grupa: studentNiz[3]
            };
            
        })).then();
    });                              
            


/*    let sadrzajCsv = req.body.toString('utf-8');             ////req.body ????
    let splittanSadrzaj = sadrzajCsv.split("\r\n");

    console.log(sadrzajCsv);

    let brDodanih = 0;
    let indeksiNedodanih = "";
    let nadjen = false;

    for (let i=0; i<splittanSadrzaj.length; i++) {
        let podaciStudenta = splittanSadrzaj[i];
        ////ili moram i podaciStudenta splittati po zarezu recimo hmmmm moguce ???????
            let podaciSplittaniPoZarezu = podaciStudenta.split(",");
            let imeS = podaciSplittaniPoZarezu[0];
            let prezimeS = podaciSplittaniPoZarezu[1];
            let indeksS = podaciSplittaniPoZarezu[2];
            let grupaS = podaciSplittaniPoZarezu[3];
            
            db.Student.findOne({
                where : {
                    index : indeksS
                }
            }).then(function(value) {
                if (value) {
                    indeksiNedodanih += (indeksS + ",");
                    nadjen = true;
                    ////sta ako grupa postoji?? --- to je slucaj da index postoji pa se ne dodaje student uopste
                }
                else {
                    ////sta ako grupa ne postoji??---- kreiramo grupu
                    db.Grupa.create({
                        grupa : grupaS
                    })
                    db.Student.create({
                        ime : imeS,
                        prezime : prezimeS,
                        index : indeksS,
                        grupa : grupaS
                    });
                    brDodanih++;
                }
            });
    }
    let objekat = {};
    if (nadjen) objekat.status = "Dodano " + brDodanih + " studenata, a studenti " + indeksiNedodanih + " već postoje!";
    else objekat.status = "Dodano " + brDodanih + " studenata";
    res.send(objekat);
}); */


//////////////// spirala 3  //////////////////////////////////////////////////////////////////////////////////////////////////////////

async function getZadaci() {
    let areja = [];
    const upitt = 'SELECT brojzadataka FROM vjezba';
    var brZad = await connection.query(upitt, {
        type : connection.QueryTypes.SELECT
    });

    for (let i=0; i<brZad.length; i++) {
        areja.push(brZad[i].brojZadataka);
    }
    return niz; 
}

 async function getVjezbe() {
    const upit = 'SELECT naziv FROM vjezba';
    var brojVjezbi= await connection.query(upit, {
        type : connection.QueryTypes.SELECT
    });
    return brojVjezbi.length; 
}

app.get('/vjezbe/', async function(req,res) {

        res.setHeader('Content-Type', 'application/json');
        var vjezbe = await getVjezbe();
        var zadaci = await getZadaci();
        res.json({brojVjezbi : vjezbe, brojZadataka : zadaci});         
}); 


app.post('/vjezbe', async function(req, res){
    let objekat1 = req.body;

    let brojVjezbi = objekat1.brojVjezbi;
    let brojZadataka = objekat1.brojZadataka;

    let greska = false;

    let tekst = "";
    let tekst1 = "";
    let tekst2 = "";
    let tekst3 = "";
    let tekstn = "";

    if (brojVjezbi > 0 && brojVjezbi < 16) {
        if (brojVjezbi == brojZadataka.length) {
            for (let i=0; i<brojZadataka.length; i++) {
                if (brojZadataka[i] > 10) {
                    tekst1 = "z" + i;
                    tekstn +=  tekst1;
                    tekstn += ",";
                }
            }
        }
        else {
            tekst2 = "Pogrešan parametar brojZadataka";
                for (let i=0; i<brojZadataka.length; i++) {
                    if (brojZadataka[i] > 10) {
                        greska = true;
                        tekst1 = "z" + i;
                        tekstn +=  tekst1;
                        tekstn += ",";
                    }
                }
            }
        }
    else {
        tekst3 = "Pogrešan parametar brojVježbi";
        if (brojVjezbi == brojZadataka.length) {
            for (let i=0; i<brojZadataka.length; i++) {
                if (brojZadataka[i] > 10) {
                    greska = true;
                    tekst1 = "z" + i;
                    tekstn +=  tekst1;
                    tekstn += ",";
                }
            }
        }
        else {
            for (let i=0; i<brojZadataka.length; i++) {
                if (brojZadataka[i] > 10) {
                    greska = true;
                    tekst1 = "z" + i;
                    tekstn +=  tekst1;
                    tekstn += ",";
                }
            }
            tekst2 = "Pogrešan parametar brojZadataka";   
        }
    }
  

    if (tekst3.toString != "") {
        tekst += tekst3;
        if (tekstn.toString != "") {
            tekst += ",";
            tekst += tekstn;
        }
        if (tekst2.toString != "") {
            tekst += tekst2;
        }
        
    }

   if (greska) {
    let objekat = {};
    objekat.status = "error";
    objekat.data = tekst;
    res.send(objekat);
   }
   else {

    let novaLinija = objekat1['brojVjezbi'];
    for (let i = 0; i<objekat1.brojZadataka.length; i++) {
            novaLinija += "," + objekat1.brojZadataka[i];
    }
}
/*    fs.writeFileSync('vjezbe.csv', novaLinija, function(err) {
        if (err) console.error(err); 
                  
    });*/
    let nizZadataka = req.body.brojZadataka.split(",");
    let temp = req.body.brojVjezbi;
    for (let i=0; i<temp; i++) {
        temp = i;
        let vjezbaa = "Vjezba " + (++temp);
        let upitn = "INSERT INTO VJEZBA(naziv, brojZadataka)VALUES('" + vjezbaa + "'" + "," + parseInt(nizZadataka[i]) + ")";
        await connection.query(upitn, {
            type : connection.QueryTypes.INSERT
        }); 
    }   
});  
     
app.listen(3000);