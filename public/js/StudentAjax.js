var StudentAjax = (function() {

    const dodajStudenta = function(student, fnCallback) {

        $.ajax({
            url:"http://localhost:3000/student",
            type: "POST",
            data: student,
            success: function(podaci){
                fnCallback(null, podaci);
            }, 
            failure: function(greska) {
                fnCallback(greska, null);
            }
        });   
    }


    const postaviGrupu = function(index, grupa, fnCallback) {

        let objekat = {grupa : grupa};

        $.ajax({
            url : "http://localhost:3000/student/" + index,
            type : "PUT",
            data : objekat,
            success : function(podaci) {
                fnCallback(null, podaci);
            },
            failure: function(greska) {
                fnCallback(greska, null);
            }
        });
    }


    const dodajBatch = function(csvStudenti, fnCallback) {
        
            $.ajax({
                url : "http://localhost:3000/batch/student",
                type : "POST",
                data : csvStudenti,
                success : function(podaci) {
                    fnCallback(null, podaci);
                },
                failure: function(greska) {
                    fnCallback(greska, null);
                }
            });
        }

    return {
        dodajStudenta:dodajStudenta,
        postaviGrupu:postaviGrupu,
        dodajBatch:dodajBatch
    }
}());