
document.getElementById("potvrdi").addEventListener('click', function(){

let div = document.getElementById("ajaxstatus");

let indeks1 = document.getElementById("index").value;
let grupa1 = document.getElementById("grupa").value;

    var validacija = new Validacija(div);
    validacija.validirajIndeks(indeks1);
    validacija.validirajGrupu(grupa1);

    if (validacija.nestoUnesenoNeispravno == true) return;

    StudentAjax.postaviGrupu(indeks1, grupa1, (err, data) => {
        document.getElementById("ajaxstatus").innerHTML = JSON.stringify(data.status);            
    });
});