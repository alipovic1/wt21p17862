let assert = chai.assert;
describe ('TestoviParser', function() {
    describe('dajTacnost()', function() {

        it('svi prolaze', function() {
            let report = "{\n\"stats\": {\n\"suites\": 2,\n\"tests\": 2,\n\"passes\": 2,\n\"pending\": 0,\n\"failures\": 0,\n\"start\": \"2021-11-05T15:00:26.343Z\",\n\"end\": \"2021-11-05T15:00:26.352Z\",\n\"duration\": 9\n},\n\"tests\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n],\n\"pending\": [],\n\"failures\": [],\n\"passes\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n]\n}";
            var prviTest = TestoviParser.dajTacnost(report);
            assert.equal(JSON.stringify(prviTest), "{\"tacnost\":\"100%\",\"greske\":[]}", 'Prolaze svi testovi');
                });

        it('neispravan json format', function() {
            let report = "{\n\"stats1\": {\n\"suites\": 2,\n\"tests\": 2,\n\"passes\": 2,\n\"pending\": 0,\n\"failures\": 0,\n\"start\": \"2021-11-05T15:00:26.343Z\",\n\"end\": \"2021-11-05T15:00:26.352Z\",\n\"duration\": 9\n},\n\"tests\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n],\n\"pending\": [],\n\"failures\": [],\n\"passes\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n]\n}";
            var vraceno = TestoviParser.dajTacnost(report);
            const vraceniString = JSON.stringify(vraceno);
            assert.equal(vraceniString, "{\"tacnost\":\"0%\",\"greske\":[\"Testovi se ne mogu izvršiti\"]}", 'Tacnost je 0%!');
            });

    it('jedan samo prolazi', function() {
        let report = "{\n\"stats\": {\n\"suites\": 2,\n\"tests\": 2,\n\"passes\": 1,\n\"pending\": 0,\n\"failures\": 1,\n\"start\": \"2021-11-05T15:00:26.343Z\",\n\"end\": \"2021-11-05T15:00:26.352Z\",\n\"duration\": 9\n},\n\"tests\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n],\n\"pending\": [],\n\"failures\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n],\n\"passes\": [\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n]\n}";
        var vraceno = TestoviParser.dajTacnost(report);
        const vraceniString = JSON.stringify(vraceno);
        assert.equal(vraceniString, "{\"tacnost\":\"50%\",\"greske\":[\"Tabela crtaj() should draw 3 rows when parameter are 2,3\"]}", 'Tacnost je 50%!');
        });

        it('svi padaju', function() {
            let report = "{\n\"stats\": {\n\"suites\": 2,\n\"tests\": 2,\n\"passes\": 0,\n\"pending\": 0,\n\"failures\": 2,\n\"start\": \"2021-11-05T15:00:26.343Z\",\n\"end\": \"2021-11-05T15:00:26.352Z\",\n\"duration\": 9\n},\n\"tests\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n],\n\"pending\": [],\n\"failures\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n],\n\"passes\": []\n}";
            var vraceno = TestoviParser.dajTacnost(report);
            const vraceniString = JSON.stringify(vraceno);
            
            var trebaBiti = "{\"tacnost\":\"0%\",\"greske\":[\"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\"]}";
            assert.equal(vraceniString, trebaBiti, 'Svi testovi padaju');
                });

        it('nedostaje neki property', function() {
            let report = "{\n\"stats\": {\n\"suites\": 2,\n\"tests\": 2,\n\"passes\": 2,\n\"pending\": 0,\n\"start\": \"2021-11-05T15:00:26.343Z\",\n\"end\": \"2021-11-05T15:00:26.352Z\",\n\"duration\": 9\n},\n\"pending\": [],\n\"passes\": [\n{\n\"title\": \"should draw 3 rows when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\n\"file\": null,\n\"duration\": 1,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n},\n{\n\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\",\n\"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\n\"file\": null,\n\"duration\": 0,\n\"currentRetry\": 0,\n\"speed\": \"fast\",\n\"err\": {}\n}\n]\n}";
            var vraceno = TestoviParser.dajTacnost(report);
            var trebaBiti = '{"tacnost":"0%","greske":["Testovi se ne mogu izvršiti"]}';
            assert.equal(JSON.stringify(vraceno), trebaBiti, 'Testovi se ne mogu izvršiti');
                });
    });
});