var Validacija = (function() {

    let errorPoruke= [];
    let nestoUnesenoNeispravno=false;

    const konstruktor = function(divElementAjaxstatus) {
		divElementAjaxstatus.innerHTML = "";

    const validirajIme = function(poslanoIme) {
        if (poslanoIme.length < 3){
            errorPoruke.push("Ime sa 2 ili manje znakova nije validno!");
            nestoUnesenoNeispravno = true;
            ispisiErrorPoruke();
        } 
    }

    const validirajPrezime = function(poslanoPrezime) {
        if ( poslanoPrezime.length < 3){
            errorPoruke.push("Prezime sa 2 ili manje znakova nije validno");
            nestoUnesenoNeispravno = true;
            ispisiErrorPoruke();
        }
    }

    const validirajIndeks = function(poslaniIndeks) {
        if (poslaniIndeks.length != 5){
            errorPoruke.push("Broj indeksa treba sadrzavati 5 znakova");
            nestoUnesenoNeispravno = true;
            ispisiErrorPoruke();
        } 
    }

    const validirajGrupu = function(poslanaGrupa) {
        if (poslanaGrupa.length < 6 || poslanaGrupa.length > 10){
            errorPoruke.push("Grupa treba imati 6 ili 7 znakova");
            nestoUnesenoNeispravno = true;
            ispisiErrorPoruke();
        } 
    }

    const validirajCsv = function(csv) {

        let splittanSadrzaj = csv.split("\r\n");

        for (let i=0; i<splittanSadrzaj.length; i++) {

            let podaciStudenta = splittanSadrzaj[i];
            ////ili moram i podaciStudenta splittati po zarezu recimo hmmmm moguce ???????
                let podaciSplittaniPoZarezu = podaciStudenta.split(",");
                let imeS = podaciSplittaniPoZarezu[0];
                let prezimeS = podaciSplittaniPoZarezu[1];
                let indeksS = podaciSplittaniPoZarezu[2];
                let grupaS = podaciSplittaniPoZarezu[3];
                if (imeS.length==0 || prezimeS.length==0 || indeksS.length==0 || grupaS.length==0) {
                    errorPoruke.push("Podaci o studentu iz reda " + (i+1) + " su nepotpuni");
                    nestoUnesenoNeispravno = true;
                }
            }
            ispisiErrorPoruke();
        }

    function ispisiErrorPoruke() {
            divElementAjaxstatus.innerHTML = errorPoruke; 
    }

    return {
        validirajIme : validirajIme,
        validirajPrezime : validirajPrezime,
        validirajIndeks : validirajIndeks,
        validirajGrupu : validirajGrupu,
        validirajCsv : validirajCsv
    }
}
return konstruktor;
}());