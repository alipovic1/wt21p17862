var TestoviParser = (function () {
    
    const dajTacnost = function(primjer) {

        let vrati = {};
        vrati.tacnost = "0%";
        vrati.greske = [];
    
            try {

            var jsObjekat = JSON.parse(primjer);
            var brojTacnih = jsObjekat.stats.passes;
            var brojTestova = jsObjekat.stats.tests;
            var ukupno = (brojTacnih/brojTestova)*100;

            if (!Number.isInteger(vrati.tacnost)) vrati.tacnost = String(Math.round(ukupno*10)/10) + "%";

        var greskeNiz = jsObjekat.failures;
        for (let i = 0; i<greskeNiz.length; i++) {
            vrati.greske[i] = greskeNiz[i].fullTitle;
        }
            } catch (err) {
                vrati.tacnost = "0%";
                vrati.greske[0] = "Testovi se ne mogu izvršiti";
            }
             return vrati; 
        
    }

    const porediRezultate = function(rezultat1, rezultat2) {

        let vrati = {};
        vrati.promjena = "0%";
        vrati.greske = [];
        
        var jsObjekat1 = JSON.parse(rezultat1);
        var jsObjekat2 = JSON.parse(rezultat2);

        if (JSON.stringify(jsObjekat1.tests) == JSON.stringify(jsObjekat2.tests)) {
            vrati.promjena = dajTacnost(rezultat2).tacnost;
            vrati.greske = dajTacnost(rezultat2).greske;
            vrati.greske.sort();
        }

        else {

            const nizNazivaPadaju1 = [];
            for (let j = 0; j < jsObjekat1.failures.length; j++) {
                nizNazivaPadaju1.push(jsObjekat1.failures[j].fullTitle); 
            }

            const padaju1NeNalaze2 = [];
            for (let s = 0; s < nizNazivaPadaju1.length; s++) {
                if (!nizNazivaRez2.includes(nizNazivaPadaju1[s])) padaju1NeNalaze2.push(nizNazivaPadaju1[s]);
            }

            var brojacBrojnik = padaju1NeNalaze2.length + jsObjekat2.stats.failures;
            var brojacNazivnik = padaju1NeNalaze2.length + jsObjekat2.tests.length;

            vrati.promjena = (brojacBrojnik/brojacNazivnik)*100 + "%";
////////////////////////////////////////////////////////////////////


            const nizNazivaRez2 = [];
            for (let d = 0; d < jsObjekat2.tests.length; d++) {
                nizNazivaRez2.push(jsObjekat2.tests[d].fullTitle);
            }

            const nizNazivaPadaju2 = [];
            for (let z = 0; z < jsObjekat2.failures.length; z++) {
                nizNazivaPadaju2.push(jsObjekat2.failures[z].fullTitle);
            }

            const nizNazivaProlaze1 = [];
            for (let g = 0; g < jsObjekat1.passes.length; g++) {
                nizNazivaProlaze1.push(jsObjekat1.passes[g].fullTitle);
            }

           const padaju2Prolaze1 = [];
           for (let p = 0; p < nizNazivaPadaju2.length; p++) {
               if (nizNazivaProlaze1.includes(nizNazivaPadaju2[p])) padaju2Prolaze1.push(nizNazivaPadaju2[p]);
           }

           const gresKSPSUR2 = [];
           for (let h = 0; h < nizNazivaProlaze1.length; h++) {
               if (padaju2Prolaze1.includes(padaju2Prolaze1[h])) gresKSPSUR2.push(padaju2Prolaze1[h]);
           }
           
           padaju1NeNalaze2.sort();
           gresKSPSUR2.sort();
           vrati.greske.push(padaju1NeNalaze2);
           vrati.greske.push(gresKSPSUR2);
        }   
        return vrati;
    }

return {
	dajTacnost:dajTacnost,
    porediRezultate: porediRezultate
    }
}());