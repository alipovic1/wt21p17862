
let VjezbeAjax = (function() {

    var dodajInputPolja = function(DOMelementDIVauFormi,brojVjezbi) {
        if (brojVjezbi < 16) {
            for (let i=0; i<brojVjezbi; i++) {
                let labela = document.createElement("label");
                let tekst = document.createTextNode("VJEŽBA" + (i+1) + "        ");
                labela.appendChild(tekst);
                let inputPolje = document.createElement("input");
                inputPolje.setAttribute("id", "z" + i);
                inputPolje.defaultValue = 4;
                let bre = document.createElement("br");
                DOMelementDIVauFormi.appendChild(labela);
                DOMelementDIVauFormi.appendChild(inputPolje);
                labela.appendChild(bre);
                inputPolje.appendChild(bre);
                DOMelementDIVauFormi.appendChild(bre);  
            }
        }
    }

    var posaljiPodatke = function(VjezbeObjekat, callbackFja) {

        let broj = VjezbeObjekat.brojZadataka;
        let duzina = broj.length;

        for (let i=0; i<duzina; i++) {
            if (broj[i] > 11 || broj[i] < 0) {
                alert("Ne moze se unijeti broj zadataka veci od 10!");
                return;
        }


        $.ajax({
            url:"http://localhost:3000/vjezbe",
            type: "POST",
            data: VjezbeObjekat,
            success: function(podaci){
                callbackFja(null, podaci);
            }, 
            failure: function(greska) {
                callbackFja(greska, null);
            }
    
        });   
    }
}

    var dohvatiPodatke = function(callbackFja) {
        $.ajax({
            url:"http://localhost:3000/vjezbe",
            type: "GET",
            success: function(podaci){
                callbackFja(null, podaci);
            }, 
            failure: function(greska) {
                callbackFja(greska, null);
            }
        });
    }

    var iscrtajVjezbe = function(divDOMelement, objekat) {

        let brojVjezbi = objekat.brojVjezbi;
        let brojZadVjezbe = objekat.brojZadataka;     


            for (let i=0; i<brojVjezbi; i++) {
                let div = document.createElement("div");
                div.setAttribute("class", "item1");
                let TextNode = document.createTextNode("VJEŽBA " + (1+i));
                div.appendChild(TextNode);
               
                divDOMelement.appendChild(div);
                let div2 = document.createElement("div");
                div2.setAttribute("class","item2");
                iscrtajZadatke(div2, brojZadVjezbe[i]);
                div.onclick = function() {
                    if (div2.getAttribute("class") == "item2") {
                        div2.setAttribute("class", "item3");
                    }
                    else if (div2.getAttribute("class") == "item3"){
                        div2.setAttribute("class", "item2");
                    }
                }
                divDOMelement.appendChild(div2);
            }
        }    
    
    var iscrtajZadatke = function(vjezbaDOMelement,brojZadataka) {

        if (brojZadataka > 0 || brojZadataka < 11) {
            for (let i=0; i<brojZadataka; i++) {
                let dugme = document.createElement("button");
                let tekst = document.createTextNode("ZADATAK" + (i+1));
                dugme.appendChild(tekst);
                vjezbaDOMelement.appendChild(dugme);
            }
        }
    }

    return {
        iscrtajVjezbe:iscrtajVjezbe,
        iscrtajZadatke:iscrtajZadatke,
        dodajInputPolja:dodajInputPolja,
        dohvatiPodatke:dohvatiPodatke,
        posaljiPodatke:posaljiPodatke
    }
}());