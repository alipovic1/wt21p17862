const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2117862", "root", "password", {host:"127.0.0.1", dialect:"mysql"});

const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//const { DataTypes } = require('sequelize');

//import modela

//db.zadatak = sequelize.import(__dirname+'/zadatak.js');
//db.vjezba = sequelize.import(__dirname+'/vjezba.js');
//db.grupa = sequelize.import(__dirname+'/grupa.js');
//db.student = sequelize.import(__dirname+'/student.js');

db.Student = require(__dirname + "/modeli/student.js")(sequelize, Sequelize.DataTypes);
db.Grupa = require(__dirname + "/modeli/grupa.js")(sequelize, Sequelize.DataTypes);
db.Vjezba = require(__dirname + "/modeli/vjezba.js")(sequelize, Sequelize.DataTypes);
db.Zadatak = require(__dirname + "/modeli/zadatak.js")(sequelize, Sequelize.DataTypes);


//relacije
db.Grupa.hasMany(db.Student, {as: 'studentiGrupe', foreignKey : 'grupa'});
db.Vjezba.hasMany(db.Zadatak, {as: 'zadaciVjezbe',  foreignKey : 'vjezba'});

sequelize.sync();

module.exports = db;
